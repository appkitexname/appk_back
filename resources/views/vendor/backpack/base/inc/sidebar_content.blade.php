<li>
    <a href="{{ backpack_url('dashboard') }}">
        <i class="fa fa-dashboard"></i> <span>Главный экран</span></a></li>

<li class="treeview">
    <a href="#"><i class="fa fa-smile-o"></i> <span>Маршруты</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('pointtype') }}"><i class="fa "></i> <span>Типы Остановок</span></a></li>
        <li><a href="{{ backpack_url('point') }}"><i class="fa "></i> <span>Остановки</span></a></li>
        <li><a href="{{ backpack_url('route') }}"><i class="fa "></i> <span>Маршруты</span></a></li>
        <li><a href="{{ backpack_url('routeschedule') }}"><i class="fa "></i> <span>Расписания</span></a></li>
    </ul>
</li>


<li class="treeview">
    <a href="#"><i class="fa fa-smile-o"></i> <span>Фирмы</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('firmowner') }}"><i class="fa "></i> <span>Бренд</span></a></li>
        <li><a href="{{ backpack_url('firmtype') }}"><i class="fa "></i> <span>Типы фирм</span></a></li>
        <li><a href="{{ backpack_url('firm') }}"><i class="fa "></i> <span>Фирмы</span></a></li>


    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-smile-o"></i> <span>Водители и транспорт</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('firmowner') }}"><i class="fa "></i> <span>Водители</span></a></li>
        <li><a href="{{ backpack_url('firmtype') }}"><i class="fa "></i> <span>Транспортные средства</span></a></li>
        <li><a href="{{ backpack_url('firmtype') }}"><i class="fa "></i> <span>Рабочие смены</span></a></li>
        <li><a href="{{ backpack_url('firmtype') }}"><i class="fa "></i> <span>Рабочие рейсы</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-smile-o"></i> <span>Рейсы и билеты</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('firmowner') }}"><i class="fa "></i> <span>Рейсы</span></a></li>
        <li><a href="{{ backpack_url('firmtype') }}"><i class="fa "></i> <span>Билеты</span></a></li>
        <li><a href="{{ backpack_url('firmtype') }}"><i class="fa "></i> <span>Бронирование</span></a></li>
    </ul>
</li>

<!-- Users, Roles Permissions -->
<li class="treeview">
  <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
    <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
    <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
    <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
  </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Advanced</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>File manager</span></a></li>
      <li><a href="{{ backpack_url('backup') }}"><i class="fa fa-hdd-o"></i> <span>Backups</span></a></li>
      <li><a href="{{ backpack_url('log') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
      <li><a href="{{ backpack_url('setting') }}"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
    </ul>
</li>