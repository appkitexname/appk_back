<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Firm extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'firms';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['firm_types_id','firm_owners_id','name','full_name','inn','ogrn','contactinfo'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function firm_type()
    {
        return $this->belongsTo('App\Models\FirmType', 'firm_types_id');
    }

    public function firm_types()
    {
        return $this->belongsTo('App\Models\FirmType', 'firm_types_id');
    }
    public function firm_owner()
    {
        return $this->belongsTo('App\Models\FirmOwner', 'firm_owners_id');
    }

    public function firm_owners()
    {
        return $this->belongsTo('App\Models\FirmOwner', 'firm_owners_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
