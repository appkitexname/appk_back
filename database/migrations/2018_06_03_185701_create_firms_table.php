<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('firm_owners_id');
            $table->foreign('firm_owners_id')->references('id')->on('firm_owners');
            $table->unsignedInteger('firm_types_id');
            $table->foreign('firm_types_id')->references('id')->on('firm_types');
            $table->string('name');
            $table->string('full_name')->nullable();
            $table->string('inn')->nullable();
            $table->string('ogrn')->nullable();
            $table->text('contactinfo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('firms');
    }
}
