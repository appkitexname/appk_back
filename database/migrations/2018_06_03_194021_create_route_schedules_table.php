<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('firm_owners_id');
            $table->foreign('firm_owners_id')->references('id')->on('firm_owners');
            $table->unsignedInteger('route_id');
            $table->foreign('route_id')->references('id')->on('routes');
            $table->date('date_start');
            $table->date('date_end');
            $table->time('time_start');
            $table->time('time_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route_schedules');
    }
}
