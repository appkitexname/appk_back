<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->insert([
         //   'name'     => 'Demo Admin',
         //   'email'    => 'admin@example.com',
         //   'password' => bcrypt('admin'),
        //]);

        $newuser = App\User::firstOrNew(array(
            'email'    => 'admin@example.com',
           // 'name'     => 'Demo Admin',
           // 'password' => bcrypt('admin'),
            ));
        $newuser->name = 'Admin';
        $newuser->password = bcrypt('admin');
        $newuser->save();
        //$newuser = App\User::firstOrNew(array(
        //    'email'    => 'admin@example.com',
        //    'name'     => 'Demo Admin',
        //    'password' => bcrypt('admin'),
        //))->save();
    }
}
