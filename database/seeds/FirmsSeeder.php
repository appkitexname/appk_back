<?php

use Illuminate\Database\Seeder;

class FirmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //$firmOwner = App\FirmOwnner::firstOrNew(array('brand_name' =>'Guests'));
        $firm = App\Models\Firm::firstOrNew(array('id'=>1));
        $firm->name='тестовая фирма';
        $firm->full_name='full_name тестовая фирма';
        $firm->firm_owners_id=2;
        $firm->firm_types_id=2;
        $firm->inn='23423423423432423';
        $firm->ogrn='3428823058345';
        $firm->contactinfo='sdsadasd';
        $firm->save();

       // $firmOwner = App\Models\FirmOwner::firstOrNew(array('id'=>2));
       // $firmOwner->brand_name='ServiceAdmins';
       // $firmOwner->save();

       // $firmOwner = App\Models\FirmOwner::firstOrNew(array('brand_name' =>'Test Owner'));
       // $firmOwner->save();
       // $firmOwner = App\FirmOwnner::firstOrNew(array('id'=>1,'brand_name' =>'ServiceAdmins'));
       // $firmOwner->save();
    }
}
