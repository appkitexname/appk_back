<?php

use Illuminate\Database\Seeder;

class FirmTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //DB::table('firm_type')->insert([['name' => 'Физлицо'],['name' => 'ИП'],['name' => 'ООО']]);
        $names = array('Физлицо','ИП','ООО','ОAО');
        foreach ($names as $n)
        {
            $firmType = App\Models\FirmType::firstOrNew(array('name' =>$n))->save();
        }

    }
}
