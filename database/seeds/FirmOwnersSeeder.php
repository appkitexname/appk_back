<?php

use Illuminate\Database\Seeder;

class FirmOwnnersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //$firmOwner = App\FirmOwnner::firstOrNew(array('brand_name' =>'Guests'));
        $firmOwner = App\Models\FirmOwner::firstOrNew(array('id'=>1));
        $firmOwner->brand_name='Guests';
        $firmOwner->save();

        $firmOwner = App\Models\FirmOwner::firstOrNew(array('id'=>2));
        $firmOwner->brand_name='ServiceAdmins';
        $firmOwner->save();

        $firmOwner = App\Models\FirmOwner::firstOrNew(array('brand_name' =>'Test Owner'));
        $firmOwner->save();
       // $firmOwner = App\FirmOwnner::firstOrNew(array('id'=>1,'brand_name' =>'ServiceAdmins'));
       // $firmOwner->save();
    }
}
